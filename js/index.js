// Tooltip
$(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

// Popover
$(function(){
    $('[data-toggle="popover"]').popover();
});

// Carousel
$(function(){
    $('.carousel').carousel({
        interval: 3000
    })
});

$('#exampleModal').on('show.bs.modal', function(e){
    $(e.relatedTarget).removeClass('btn-primary');
    $(e.relatedTarget).addClass('btn-success');
    $(e.relatedTarget).prop('disabled', true);
});

$('#exampleModal').on('shown.bs.modal', function(e){
    console.log('se mostró');
});

$('#exampleModal').on('hide.bs.modal', function(e){
    console.log('se cierra');
});

$('#exampleModal').on('hidden.bs.modal', function(e){            
    $('.btn-success').prop('disabled', false);
    $('.btn-success.btnModal').addClass('btn-primary');
    $('.btn-success.btnModal').removeClass('btn-success');
});